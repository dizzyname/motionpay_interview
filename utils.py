import uuid
import json
from bson import ObjectId
from datetime import datetime


def generate_uuid():
    return str(uuid.uuid1())


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


def transfer_balance(coll, sender, amount, remarks, receiver):
    query_receiver = coll.find({"user_id": receiver})[0]
    query_sender = coll.find({"phone_number": sender})[0]
    balance = query_receiver["balance"]
    transaction = query_receiver["transaction"]

    result = {
        "transfer_id": generate_uuid(),
        "sender_id": query_sender["user_id"],
        "amount": amount,
        "remarks": remarks,
        "balance_before": balance,
        "balance_after": balance + amount,
        "created_date": datetime.now().strftime("%Y-%m-%d %X"),
    }
    response = {"status": "STATUS", "result": result}
    if transaction == None:
        transaction = [result]
    else:
        transaction.append(result)

    coll.update_one(
        {"_id": query_receiver["_id"]},
        {
            "$set": {
                "balance": balance + amount,
                "transaction": transaction,
            }
        },
    )
    return response


if __name__ == "__main__":
    print(generate_uuid())
