from flask import Flask, request, jsonify
from datetime import datetime
from flask_jwt_extended import *

from utils import generate_uuid, JSONEncoder, transfer_balance
from config import Config

import pymongo
import json


client = pymongo.MongoClient(
    "mongodb+srv://motionpay:R9EKeifaFechXrTa@cluster0.rhwxu.mongodb.net/motiondb?retryWrites=true&w=majority"
)
db = client[Config.DATABASE]
customers_collection = db[Config.COLLECTION]

app = Flask(__name__)
app.config["JWT_SECRET_KEY"] = Config.JWT_SECRET_KEY
jwt = JWTManager(app)


@app.route("/register", methods=["POST"])
def register():
    response = {}
    data = json.loads(request.data)
    print("Data", data)
    data["user_id"] = generate_uuid()
    data["created_date"] = datetime.now().strftime("%Y-%m-%d %X")
    data["balance"] = 0
    data["transaction"] = []

    try:
        inserted_id = customers_collection.insert_one(data)
        print("Inserted_ID", inserted_id)
        response["status"] = "SUCCESS"
        response["result"] = data
        return JSONEncoder().encode(response)
    except:
        return {"message": "Phone Number already registered"}


@app.route("/login", methods=["POST"])
def login():
    phone_number = request.json.get("phone_number", None)
    pin = request.json.get("pin", None)

    query = customers_collection.find({"phone_number": phone_number})[0]
    if pin != query["pin"]:
        return {"message": "Phone number and pin doesn’t match"}
    else:
        access_token = create_access_token(identity=phone_number)
        refresh_token = create_refresh_token(identity=phone_number)
        response = {
            "status": "SUCCESS",
            "result": {
                "access_token": access_token,
                "refresh_token": refresh_token,
            },
        }
    return jsonify(response)


@app.route("/topup", methods=["POST"])
@jwt_required()
def topup():
    amount = request.json.get("amount", None)
    current_user = get_jwt_identity()
    query_user = customers_collection.find({"phone_number": current_user})[0]
    balance = query_user["balance"]
    transaction = query_user["transaction"]
    result = {
        "top_up_id": generate_uuid(),
        "amount_top_up": int(amount),
        "balance_before": balance,
        "balance_after": balance + amount,
        "created_date": datetime.now().strftime("%Y-%m-%d %X"),
    }
    if transaction == None:
        transaction = [result]
    else:
        transaction.append(result)
    print(transaction)
    # print(transaction.append(result))
    customers_collection.update_one(
        {"_id": query_user["_id"]},
        {
            "$set": {
                "balance": balance + amount,
                "transaction": transaction,
            }
        },
    )
    response = {
        "status": "SUCCESS",
        "result": result,
    }
    return jsonify(response)


@app.route("/pay", methods=["POST"])
@jwt_required()
def payment():
    current_user = get_jwt_identity()
    amount = request.json.get("amount", None)
    remarks = request.json.get("remarks", None)
    query_user = customers_collection.find({"phone_number": current_user})[0]
    balance = query_user["balance"]
    transaction = query_user["transaction"]

    if balance >= amount:
        result = {
            "payment_id": generate_uuid(),
            "amount": int(amount),
            "remarks": remarks,
            "balance_before": balance,
            "balance_after": balance - amount,
            "created_date": datetime.now().strftime("%Y-%m-%d %X"),
        }
        if transaction == None:
            transaction = [result]
        else:
            transaction.append(result)

        customers_collection.update_one(
            {"_id": query_user["_id"]},
            {
                "$set": {
                    "balance": balance - amount,
                    "transaction": transaction,
                }
            },
        )
        response = {
            "status": "SUCCESS",
            "result": result,
        }
        return jsonify(response)

    else:
        return {"message": "Balance is not enough"}


@app.route("/transfer", methods=["POST"])
@jwt_required()
def transfer():
    current_user = get_jwt_identity()
    amount = request.json.get("amount", None)
    remarks = request.json.get("remarks", None)
    target_user = request.json.get("target_user", None)
    query_user = customers_collection.find({"phone_number": current_user})[0]
    balance = query_user["balance"]
    transaction = query_user["transaction"]

    if balance >= amount:
        result = {
            "transfer_id": generate_uuid(),
            "amount": int(amount),
            "remarks": remarks,
            "balance_before": balance,
            "balance_after": balance - amount,
            "created_date": datetime.now().strftime("%Y-%m-%d %X"),
        }
        if transaction == None:
            transaction = [result]
        else:
            transaction.append(result)

        customers_collection.update_one(
            {"_id": query_user["_id"]},
            {
                "$set": {
                    "balance": balance - amount,
                    "transaction": transaction,
                }
            },
        )
        transfer_balance(
            customers_collection, current_user, amount, remarks, target_user
        )
        response = {
            "status": "SUCCESS",
            "result": result,
        }
        return jsonify(response)

    else:
        return {"message": "Balance is not enough"}


@app.route("/transactions", methods=["GET"])
@jwt_required()
def transactions():
    current_user = get_jwt_identity()
    query_user = customers_collection.find({"phone_number": current_user})[0]
    transactions = query_user["transaction"]
    user_id = query_user["user_id"]
    new_transactions = []
    for transaction in transactions:
        transaction["user_id"] = user_id
        transaction["status"] = "SUCCESS"
        if "top_up_id" in transaction:
            transaction["transaction_type"] = "CREDIT"
        else:
            transaction["transaction_type"] = "DEBIT"
        new_transactions.append(transaction)
    response = {"status": "SUCCESS", "result": new_transactions}

    return response


@app.route("/profile", methods=["PUT"])
@jwt_required()
def profile():
    current_user = get_jwt_identity()
    first_name = request.json.get("first_name", None)
    last_name = request.json.get("last_name", None)
    address = request.json.get("address", None)
    query_user = customers_collection.find({"phone_number": current_user})[0]
    user_id = query_user["user_id"]

    result = {
        "user_id": user_id,
        "first_name": first_name,
        "last_name": last_name,
        "address": address,
        "update_date": datetime.now().strftime("%Y-%m-%d %X"),
    }

    customers_collection.update_one(
        {"_id": query_user["_id"]},
        {
            "$set": {
                "first_name": first_name,
                "last_name": last_name,
                "address": address,
            }
        },
    )

    response = {"status": "SUCCESS", "result": result}

    return response


if __name__ == "__main__":
    app.run(debug=True)
